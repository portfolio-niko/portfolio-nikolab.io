---
title: "Profile"
date: 2021-12-15T14:38:43+01:00
draft: false
---


  
Hello there! I'm Niko, a "full-stack" software engineer  
   
I currently work as a researcher in the field of Industry 4.0 and automation 
at the University of Applied Sciences and Arts of Southern Switzerland.

Start navigating my Portfolio by clicking on the following folders.

Thanks for visiting!   