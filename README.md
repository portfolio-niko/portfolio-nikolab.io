### Info

Important to pull also the theme as a submodule:
```
git submodule update --init --recursive
```